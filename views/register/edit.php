<?php
include_once("../../" . "vendor/autoload.php");
use \App\Registration\Registration;
use \App\Utility\Utility;

$obj = new Registration();
$var = $obj->edit($_GET['id']);
?>
<html>
       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <link href="../../resource/css/bootstrap.min.css" rel="stylesheet">
              <title>Registration</title>
       </head>

       <body>
              <div class="container">
                     <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                   <h3>Registration</h3>
                            </div>
                     </div>
                     <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                   <form role="form" action="update.php" method="post">
                                          <input type="hidden" name="id" value="<?php echo $var->id; ?>">
                                          <div class="form-group">
                                                 <label for="name">Name:</label>
                                                 <input type="text" name="name" value= "<?php echo $var->name; ?>" class="form-control" id="name">
                                          </div>
                                          <div class="form-group">
                                                 <label for="birthday">Birthday:</label>
                                                 <input type="date" name="birthday" value = "<?php echo $var->birthday; ?>"  class="form-control" id="birthday">
                                          </div>
                                          <div class="form-group">
                                                 <label for="email">Email:</label>
                                                 <input type="email" name="email" value = "<?php echo $var->email; ?>" class="form-control" id="email">
                                          </div>
                                          <div class="form-group">
                                                 <label for="mobile">Mobile#</label>
                                                 <input type="text" name="mobile" pattern="[+]{1}[0-9]{13}" value = "<?php echo $var->mobile; ?>" class="form-control" id="mobile" placeholder="+880**********">
                                          </div>
                                          <div class="form-group">
                                                 <label class="control-label col-sm-2" for="country">Country:</label>
                                                 <div class="col-sm-10">          
                                                        <select class="form-control" name="country" id="country" >
                                                               <option value='Bangladesh' <?php if($var->country == 'Bangladesh') echo "selected" ?> >Bangladesh</option>
                                                               <option value='Spain' <?php if($var->country == 'Spain') echo "selected" ?> >Spain</option>
                                                               <option value='Dubai' <?php if($var->country == 'Dubai') echo "selected" ?> >Dubai</option>
                                                               <option value='Canada' <?php if($var->country == 'Canada') echo "selected" ?> >Canada</option>
                                                        </select>
                                                 </div>
                                          </div>
                                          <div class="form-group">  
                                                 <label class="col-sm-2" for="gender">Gender:</label>
                                                 <div class="radio col-sm-10">
                                                        <label><input type="radio" name="gender" value="Male" <?php if($var->gender=='Male') echo "checked" ?> >Male</label>
                                                        <label><input type="radio" name="gender" value="Female" <?php if($var->gender=='Female') echo "checked" ?> >Female</label>
                                                 </div>
                                          </div>
                                          <div class="form-group">  
                                                 <label class="col-sm-2" for="term">Term:</label>
                                                 <div class="radio col-sm-10">
                                                        <label><input type="radio" name="term" value="Agree" <?php if($var->term=='Agree') echo "checked" ?> >Agree</label>
                                                        <label><input type="radio" name="term" value="Not Agree" <?php if($var->term=='Not Agree') echo "checked" ?> >Not Agree</label>
                                                 </div>
                                          </div>    

                                          <div class="btn-group">
                                                 <button type="submit" class="btn btn-info">Submit</button>
                                                 <button type="submit" class="btn btn-warning"><a href="index.php">List</a></button>
                                          </div>
                                   </form>
                            </div>
                     </div>
              </div>
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <script src="../../resource/js/bootstrap.min.js"></script>
       </body>
</html>